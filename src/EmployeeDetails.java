
import net.miginfocom.swing.MigLayout;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.DecimalFormat;
import java.util.Random;
import java.util.Vector;

class EmployeeDetails extends JFrame implements ActionListener, ItemListener, DocumentListener, WindowListener {
    private static final DecimalFormat format;
    static {
        format = new DecimalFormat("\u20ac ###,###,##0.00");
    }
    private static final DecimalFormat fieldFormat;
    static {
        fieldFormat = new DecimalFormat("0.00");
    }
    private static final EmployeeDetails frame;
    static {
        frame = new EmployeeDetails();
    }

    private JMenuItem open;
    private JMenuItem save;
    private JMenuItem saveAs;
    private JMenuItem create;
    private JMenuItem modify;
    private JMenuItem delete;
    private JMenuItem firstItem;
    private JMenuItem lastItem;
    private JMenuItem nextItem;
    private JMenuItem prevItem;
    private JMenuItem searchById;
    private JMenuItem searchBySurname;
    private JMenuItem listAll;
    private JMenuItem closeApp;
    private JButton first;
    private JButton previous;
    private JButton next;
    private JButton last;
    private JButton add;
    private JButton edit;
    private JButton deleteButton;
    private JButton displayAll;
    private JButton searchId;
    private JButton searchSurname;
    private JButton saveChange;
    private JButton cancelChange;
    private JComboBox<String> genderCombo;
    private JComboBox<String> departmentCombo;
    private JComboBox<String> fullTimeCombo;
    private JTextField idField;
    private JTextField ppsField;
    private JTextField surnameField;
    private JTextField firstNameField;
    private JTextField salaryField;
    private long currentByteStart;
    private RandomFile application;
    private FileNameExtensionFilter dataFilter;
    private File file;
    private boolean change;
    boolean changesMade;
      Font font1;
      String generatedFileName;
      Employee currentEmployee;
      JTextField searchByIdField;
      JTextField searchBySurnameField;
      String[] gender;
      String[] department;
      String[] fullTime;


    private EmployeeDetails() {
        currentByteStart = 0;
        application = new RandomFile();
        dataFilter = new FileNameExtensionFilter("dat files (*.dat)", "dat");
        change = false;
        changesMade = false;
        font1 = new Font("SansSerif", Font.BOLD, 16);
        gender = new String[]{ "", "M", "F" };
        department = new String[]{ "", "Management" , "Production", "Transport", "Management" };
        fullTime = new String[]{ "", "Yes", "No" };
    }

    private JMenuBar menuBar() {
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu, recordMenu, navigateMenu, closeMenu;
        fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);
        recordMenu = new JMenu("Records");
        recordMenu.setMnemonic(KeyEvent.VK_R);
        navigateMenu = new JMenu("Navigate");
        navigateMenu.setMnemonic(KeyEvent.VK_N);
        closeMenu = new JMenu("Exit");
        closeMenu.setMnemonic(KeyEvent.VK_E);
        menuBar.add(fileMenu);
        menuBar.add(recordMenu);
        menuBar.add(navigateMenu);
        menuBar.add(closeMenu);
        getMenu(fileMenu, recordMenu, navigateMenu, closeMenu);
        return menuBar;
    }

    private void getMenu(JMenu fileMenu, JMenu recordMenu, JMenu navigateMenu, JMenu closeMenu) {
        fileMenu.add(open = new JMenuItem("Open")).addActionListener(this);
        open.setMnemonic(KeyEvent.VK_O);
        open.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
        fileMenu.add(save = new JMenuItem("Save")).addActionListener(this);
        save.setMnemonic(KeyEvent.VK_S);
        save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        fileMenu.add(saveAs = new JMenuItem("Save As")).addActionListener(this);
        saveAs.setMnemonic(KeyEvent.VK_F2);
        saveAs.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, ActionEvent.CTRL_MASK));

        recordMenu.add(create = new JMenuItem("Create new Record")).addActionListener(this);
        create.setMnemonic(KeyEvent.VK_N);
        create.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        recordMenu.add(modify = new JMenuItem("Modify Record")).addActionListener(this);
        modify.setMnemonic(KeyEvent.VK_E);
        modify.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
        recordMenu.add(delete = new JMenuItem("Delete Record")).addActionListener(this);

        navigateMenu.add(firstItem = new JMenuItem("First"));
        firstItem.addActionListener(this);
        navigateMenu.add(prevItem = new JMenuItem("Previous"));
        prevItem.addActionListener(this);
        navigateMenu.add(nextItem = new JMenuItem("Next"));
        nextItem.addActionListener(this);
        navigateMenu.add(lastItem = new JMenuItem("Last"));
        lastItem.addActionListener(this);
        navigateMenu.addSeparator();
        navigateMenu.add(searchById = new JMenuItem("Search by ID")).addActionListener(this);
        navigateMenu.add(searchBySurname = new JMenuItem("Search by Surname")).addActionListener(this);
        navigateMenu.add(listAll = new JMenuItem("List all Records")).addActionListener(this);

        closeMenu.add(closeApp = new JMenuItem("Close")).addActionListener(this);
        closeApp.setMnemonic(KeyEvent.VK_F4);
        closeApp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, ActionEvent.CTRL_MASK));
    }

    private JPanel searchPanel() {
        JPanel searchPanel = new JPanel(new MigLayout());
        searchPanel.setBorder(BorderFactory.createTitledBorder("Search"));
        searchPanel.add(new JLabel("Search by ID:"), "growx, growx");
        searchPanel.add(searchByIdField = new JTextField(20), "width 200:200:200, growx, growx");
                        searchByIdField.addActionListener(this);
                        searchByIdField.setDocument(new JTextFieldLimit(20));
        searchPanel.add(searchId = new JButton(new ImageIcon(new ImageIcon("imgres.png").getImage().getScaledInstance(35, 20, java.awt.Image.SCALE_SMOOTH))), "width 35:35:35, height 20:20:20, growx, pushx, wrap");
                        searchId.addActionListener(this);
                        searchId.setToolTipText("Search Employee By ID");
        searchPanel.add(new JLabel("Search by Surname:"), "growx, pushx");
        searchPanel.add(searchBySurnameField = new JTextField(20), "width 200:200:200, growx, pushx");
                        searchBySurnameField.addActionListener(this);
                        searchBySurnameField.setDocument(new JTextFieldLimit(20));
        searchPanel.add(searchSurname = new JButton(new ImageIcon(new ImageIcon("imgres.png").getImage().getScaledInstance(35, 20, java.awt.Image.SCALE_SMOOTH))), "width 35:35:35, height 20:20:20, growx, growx, wrap");
                        searchSurname.addActionListener(this);
                        searchSurname.setToolTipText("Search Employee By Surname");

        return searchPanel;
    }

    private JPanel navigPanel() {
        JPanel navigPanel = new JPanel();
        navigPanel.setBorder(BorderFactory.createTitledBorder("Navigate"));
        navigPanel.add(first = new JButton(new ImageIcon(
                new ImageIcon("first.png").getImage().getScaledInstance(17, 17, java.awt.Image.SCALE_SMOOTH))));
        first.setPreferredSize(new Dimension(17, 17));
        first.addActionListener(this);
        first.setToolTipText("Display first Record");

        navigPanel.add(previous = new JButton(new ImageIcon(new ImageIcon("previous.png").getImage()
                .getScaledInstance(17, 17, java.awt.Image.SCALE_SMOOTH))));
        previous.setPreferredSize(new Dimension(17, 17));
        previous.addActionListener(this);
        previous.setToolTipText("Display next Record");

        navigPanel.add(next = new JButton(new ImageIcon(
                new ImageIcon("next.png").getImage().getScaledInstance(17, 17, java.awt.Image.SCALE_SMOOTH))));
        next.setPreferredSize(new Dimension(17, 17));
        next.addActionListener(this);
        next.setToolTipText("Display previous Record");

        navigPanel.add(last = new JButton(new ImageIcon(
                new ImageIcon("last.png").getImage().getScaledInstance(17, 17, java.awt.Image.SCALE_SMOOTH))));
        last.setPreferredSize(new Dimension(17, 17));
        last.addActionListener(this);
        last.setToolTipText("Display last Record");

        return navigPanel;
    }

    private JPanel buttonPanel() {
        JPanel buttonPanel = new JPanel();
        final String constraints = "grow, push";
        buttonPanel.add(add = new JButton("Add Record"), constraints);
        add.addActionListener(this);
        add.setToolTipText("Add new Employee Record");
        buttonPanel.add(edit = new JButton("Edit Record"), constraints);
        edit.addActionListener(this);
        edit.setToolTipText("Edit current Employee");
        buttonPanel.add(deleteButton = new JButton("Delete Record"), "grow, push, wrap");
        deleteButton.addActionListener(this);
        deleteButton.setToolTipText("Delete current Employee");
        buttonPanel.add(displayAll = new JButton("List all Records"), constraints);
        displayAll.addActionListener(this);
        displayAll.setToolTipText("List all Registered Employees");

        return buttonPanel;
    }

    private JPanel detailsPanel() {
        JPanel empDetails = new JPanel(new MigLayout());
        JPanel buttonPanel = new JPanel();
        empDetails.setBorder(BorderFactory.createTitledBorder("Employee Details"));
        final String s1 = "grow, push, wrap";
        final String s2 = "grow, push";
        empDetails.add(new JLabel("ID:"), s2);
        empDetails.add(idField = new JTextField(20), s1);
        idField.setEditable(false);
        empDetails.add(new JLabel("PPS Number:"), s2);
        empDetails.add(ppsField = new JTextField(20), s1);
        empDetails.add(new JLabel("Surname:"), s2);
        empDetails.add(surnameField = new JTextField(20), s1);
        empDetails.add(new JLabel("First Name:"), s2);
        empDetails.add(firstNameField = new JTextField(20), s1);
        empDetails.add(new JLabel("Gender:"), s2);
        empDetails.add(genderCombo = new JComboBox<>(gender), s1);
        empDetails.add(new JLabel("Department:"), s2);
        empDetails.add(departmentCombo = new JComboBox<>(department), s1);
        empDetails.add(new JLabel("Salary:"), s2);
        empDetails.add(salaryField = new JTextField(20), s1);
        empDetails.add(new JLabel("Full Time:"), s2);
        empDetails.add(fullTimeCombo = new JComboBox<>(fullTime), s1);
        buttonPanel.add(saveChange = new JButton("Save"));
                        saveChange.addActionListener(this);
                        saveChange.setVisible(false);
                        saveChange.setToolTipText("Save changes");
        buttonPanel.add(cancelChange = new JButton("Cancel"));
                        cancelChange.addActionListener(this);
                        cancelChange.setVisible(false);
                        cancelChange.setToolTipText("Cancel edit");
        empDetails.add(buttonPanel, "span 2 ,grow, push,wrap");
        for (int i = 0; i < empDetails.getComponentCount(); i++) {
            empDetails.getComponent(i).setFont(font1);
            addFormat(empDetails, i);
        }
        return empDetails;
    }

    private void addFormat(JPanel empDetails, int i) {
        JTextField field;
        if (empDetails.getComponent(i) instanceof JTextField) {
            field = (JTextField) empDetails.getComponent(i);
            field.setEditable(false);
            if (field == ppsField)
                field.setDocument(new JTextFieldLimit(9));
            else
                field.setDocument(new JTextFieldLimit(20));
            field.getDocument().addDocumentListener(this);
        }
        else if (empDetails.getComponent(i) instanceof JComboBox) {
            empDetails.getComponent(i).setBackground(Color.WHITE);
            empDetails.getComponent(i).setEnabled(false);
            ((JComboBox<String>) empDetails.getComponent(i)).addItemListener(this);
            ((JComboBox<String>) empDetails.getComponent(i)).setRenderer(new DefaultListCellRenderer() {
                public void paint(Graphics g) {
                    setForeground(new Color(65, 65, 65));
                    super.paint(g);
                }
            });
        }
    }

    public void displayRecords(Employee thisEmployee) {
        int countGender = 0;
        int countDep = 0;
        searchByIdField.setText("");
        searchBySurnameField.setText("");
        displayEmployeeDetails(thisEmployee, countGender, countDep, false);
        change = false;
    }

    private void displayEmployeeDetails(Employee thisEmployee, int countGender, int countDep, boolean found) {
        if (thisEmployee == null || thisEmployee.getEmployeeId() == 0 ) {
        } else {
            while (!found && countGender < gender.length - 1) {
                if (Character.toString(thisEmployee.getGender()).equalsIgnoreCase(gender[countGender]))
                    found = true;
                else
                    countGender++;
            }
            found = false;
            while (!found && countDep < department.length - 1) {
                if (thisEmployee.getJob().getDepartment().trim().equalsIgnoreCase(department[countDep]))
                    found = true;
                else
                    countDep++;
            }
            idField.setText(Integer.toString(thisEmployee.getEmployeeId()));
            ppsField.setText(thisEmployee.getPps().trim());
            surnameField.setText(thisEmployee.getSurname().trim());
            firstNameField.setText(thisEmployee.getFirstName());
            genderCombo.setSelectedIndex(countGender);
            departmentCombo.setSelectedIndex(countDep);
            salaryField.setText(format.format(thisEmployee.getJob().getSalary()));
            if (thisEmployee.getJob().getFullTime())
                fullTimeCombo.setSelectedIndex(1);
            else
                fullTimeCombo.setSelectedIndex(2);
        }
    }

    private void displayEmployeeSummaryDialog() {
        if (isSomeoneToDisplay())
            new EmployeeSummaryDialog(getAllEmployees());
    }
    private void displaySearchByIdDialog() {
        if (isSomeoneToDisplay())
            new SearchByIdDialog(EmployeeDetails.this);
    }
    private void displaySearchBySurnameDialog() {
        if (isSomeoneToDisplay())
            new SearchBySurnameDialog(EmployeeDetails.this);
    }

    private void firstRecord() {
        if (isSomeoneToDisplay()) {
            application.openReadFile(file.getAbsolutePath());
            currentByteStart = application.getFirst();
            currentEmployee = application.readRecords(currentByteStart);
            application.closeReadFile();
            if (currentEmployee.getEmployeeId() == 0)
                nextRecord();
        }
    }

    private void previousRecord() {
        if (isSomeoneToDisplay()) {
            application.openReadFile(file.getAbsolutePath());
            currentByteStart = application.getPrevious(currentByteStart);
            currentEmployee = application.readRecords(currentByteStart);
            while (currentEmployee.getEmployeeId() == 0) {
                currentByteStart = application.getPrevious(currentByteStart);
                currentEmployee = application.readRecords(currentByteStart);
            }
            application.closeReadFile();
        }
    }

    private void nextRecord() {
        if (isSomeoneToDisplay()) {
            application.openReadFile(file.getAbsolutePath());
            currentByteStart = application.getNext(currentByteStart);
            currentEmployee = application.readRecords(currentByteStart);
            while (currentEmployee.getEmployeeId() == 0) {
                currentByteStart = application.getNext(currentByteStart);
                currentEmployee = application.readRecords(currentByteStart);
            }
            application.closeReadFile();
        }
    }
    private void lastRecord() {
        if (isSomeoneToDisplay()) {
            application.openReadFile(file.getAbsolutePath());
            currentByteStart = application.getLast();
            currentEmployee = application.readRecords(currentByteStart);
            application.closeReadFile();
            if (currentEmployee.getEmployeeId() == 0)
                previousRecord();
        }
    }
    public void searchEmployeeById() {
        boolean found = false;
        try {
            searchEmployeeID(found);
        }
        catch (NumberFormatException e) {
            searchByIdField.setBackground(new Color(255, 150, 150));
            JOptionPane.showMessageDialog(null, "Wrong ID format!" );
        }
        searchByIdField.setBackground(Color.WHITE);
        searchByIdField.setText("");
    }

    private void searchEmployeeID(boolean found) {
        if (isSomeoneToDisplay()) {
            firstRecord();
            int firstId = currentEmployee.getEmployeeId();
            found = idIsFound(found, firstId);
            if (!found)
                JOptionPane.showMessageDialog(null, "Employee not found!");
        }
    }

    private boolean idIsFound(boolean found, int firstId) {
        final String searchID = searchByIdField.getText().trim();
        final String currentEmployeeID = Integer.toString(currentEmployee.getEmployeeId());
        final String id = idField.getText().trim();

        if (searchID.equals(id)) {found = true;}

        if (searchID.equals(currentEmployeeID)) {
            found = true;
            displayRecords(currentEmployee);
        } else {
            nextRecord();
            while (firstId != currentEmployee.getEmployeeId()) {
                if (Integer.parseInt(searchID) == currentEmployee.getEmployeeId()) {
                    found = true;
                    displayRecords(currentEmployee);
                    break;
                } else
                    nextRecord();
            }
        }
        return found;
    }


    public void searchEmployeeBySurname() {
        boolean found = false;
        searchEmployeeSurname(found);
        searchBySurnameField.setText("");
    }
    private void searchEmployeeSurname(boolean found) {
        if (isSomeoneToDisplay()) {
            firstRecord();
            String firstSurname = currentEmployee.getSurname().trim();
            found = surnameIsFound(found, firstSurname);
            if (!found)
                JOptionPane.showMessageDialog(null, "Employee not found!");
        }
    }
    private boolean surnameIsFound(boolean found, String firstSurname) {
        final String searchSurname = searchBySurnameField.getText().trim();
        final String surname = surnameField.getText().trim();
        if (searchSurname.equalsIgnoreCase(surname)) {found = true;}
        if (searchSurname.equalsIgnoreCase(currentEmployee.getSurname().trim())) {
            found = true;
            displayRecords(currentEmployee);
        } else {
            nextRecord();
            while (!firstSurname.trim().equalsIgnoreCase(currentEmployee.getSurname().trim())) {
                if (searchSurname.equalsIgnoreCase(currentEmployee.getSurname().trim())) {
                    found = true;
                    displayRecords(currentEmployee);
                    break;
                }
                else
                    nextRecord();
            }
        }
        return found;
    }

    public int getNextFreeId() {
        int nextFreeId = 0;
        nextFreeId = getNextFreeId(nextFreeId);
        return nextFreeId;
    }

    private int getNextFreeId(int nextFreeId) {
        if (file.length() == 0 || !isSomeoneToDisplay())
            nextFreeId++;
        else {
            lastRecord();
            nextFreeId = currentEmployee.getEmployeeId() + 1;
        }
        return nextFreeId;
    }

    private Employee getChangedDetails() {
        boolean fullTime = false;
        Employee theEmployee;
        if (((String) fullTimeCombo.getSelectedItem()).equalsIgnoreCase("Yes"))
            fullTime = true;

        theEmployee = new Employee(Integer.parseInt(idField.getText()), ppsField.getText().toUpperCase(),
                surnameField.getText().toUpperCase(), firstNameField.getText().toUpperCase(),
                genderCombo.getSelectedItem().toString().charAt(0),  new Job(departmentCombo.getSelectedItem().toString(),
                Double.parseDouble(salaryField.getText()), fullTime));

        return theEmployee;
    }

    public void addRecord(Employee newEmployee) {
        application.openWriteFile(file.getAbsolutePath());
        currentByteStart = application.addRecords(newEmployee);
        application.closeWriteFile();
    }

    private void deleteRecord() {
        if (isSomeoneToDisplay()) {
            int returnVal = JOptionPane.showOptionDialog(frame, "Do you want to delete record?", "Delete",
                    JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
            if (returnVal == JOptionPane.YES_OPTION) {
                application.openWriteFile(file.getAbsolutePath());
                application.deleteRecords(currentByteStart);
                application.closeWriteFile();
                if (isSomeoneToDisplay()) {
                    nextRecord();
                    displayRecords(currentEmployee);
                }
            }
        }
    }

    private Vector<Object> getAllEmployees() {
        Vector<Object> allEmployee = new Vector<>();
        Vector<Object> empDetails;
        long byteStart = currentByteStart;
        firstRecord();
        int firstId = currentEmployee.getEmployeeId();
        do {
            empDetails = new Vector<>();
            empDetails.addElement(currentEmployee.getEmployeeId());
            empDetails.addElement(currentEmployee.getPps());
            empDetails.addElement(currentEmployee.getSurname());
            empDetails.addElement(currentEmployee.getFirstName());
            empDetails.addElement(currentEmployee.getGender());
            empDetails.addElement(currentEmployee.getJob().getDepartment());
            empDetails.addElement(currentEmployee.getJob().getSalary());
            empDetails.addElement(currentEmployee.getJob().getFullTime());
            allEmployee.addElement(empDetails);
            nextRecord();
        } while (firstId != currentEmployee.getEmployeeId());
        currentByteStart = byteStart;
        return allEmployee;
    }

    private void editDetails() {
        if (isSomeoneToDisplay()) {
            final double salary = currentEmployee.getJob().getSalary();
            salaryField.setText(fieldFormat.format(salary));
            change = false;
            setEnabled(true);
        }
    }

    private void cancelChange() {
        setEnabled(false);
        displayRecords(currentEmployee);
    }
    private boolean isSomeoneToDisplay() {
        boolean someoneToDisplay;
        application.openReadFile(file.getAbsolutePath());
        someoneToDisplay = application.isSomeoneToDisplay();
        application.closeReadFile();
        if (!someoneToDisplay) {
            currentEmployee = null;
            idField.setText("");
            ppsField.setText("");
            surnameField.setText("");
            firstNameField.setText("");
            salaryField.setText("");
            genderCombo.setSelectedIndex(0);
            departmentCombo.setSelectedIndex(0);
            fullTimeCombo.setSelectedIndex(0);
            JOptionPane.showMessageDialog(null, "No Employees registered!");
        }
        return someoneToDisplay;
    }

    public boolean correctPps(String pps, long currentByte) {

        return ppsFound(pps, currentByte);
    }

    private boolean ppsFound(String pps, long currentByte) {
        boolean ppsExist;
        if (pps.length() == 8 || pps.length() == 9) {
            if (Character.isDigit(pps.charAt(0)) && Character.isDigit(pps.charAt(1))
                    && Character.isDigit(pps.charAt(2))	&& Character.isDigit(pps.charAt(3))
                    && Character.isDigit(pps.charAt(4))	&& Character.isDigit(pps.charAt(5))
                    && Character.isDigit(pps.charAt(6))	&& Character.isLetter(pps.charAt(7))
                    && (pps.length() == 8 || Character.isLetter(pps.charAt(8)))) {
                application.openReadFile(file.getAbsolutePath());
                ppsExist = application.isPpsExist(pps, currentByte);
                application.closeReadFile();
            }
            else
                ppsExist = true;
        }
        else
            ppsExist = true;
        return ppsExist;
    }

    private boolean checkFileName(File fileName) {
        boolean checkFile = false;
        int length = fileName.toString().length();
        if (fileName.toString().charAt(length - 4) == '.' && fileName.toString().charAt(length - 3) == 'd'
                && fileName.toString().charAt(length - 2) == 'a' && fileName.toString().charAt(length - 1) == 't')
            checkFile = true;
        return checkFile;
    }

    private boolean checkForChanges() {
        boolean anyChanges = false;
        if (change) {
            saveChanges();
            anyChanges = true;
        } else {
            setEnabled(false);
            displayRecords(currentEmployee);
        }
        return anyChanges;
    }

    private boolean checkInput() {
        boolean valid = true;

        final Color bg = new Color(255, 150, 150);
        if (ppsField.isEditable() && ppsField.getText().trim().isEmpty()) {
            ppsField.setBackground(bg);
            valid = false;
        }
        if (ppsField.isEditable() && correctPps(ppsField.getText().trim(), currentByteStart)) {
            ppsField.setBackground(bg);
            valid = false;
        }
        if (surnameField.isEditable() && surnameField.getText().trim().isEmpty()) {
            surnameField.setBackground(bg);
            valid = false;
        }
        if (firstNameField.isEditable() && firstNameField.getText().trim().isEmpty()) {
            firstNameField.setBackground(bg);
            valid = false;
        }
        if (genderCombo.getSelectedIndex() == 0 && genderCombo.isEnabled()) {
            genderCombo.setBackground(bg);
            valid = false;
        }
        if (departmentCombo.getSelectedIndex() == 0 && departmentCombo.isEnabled()) {
            departmentCombo.setBackground(bg);
            valid = false;
        }
        try {
            Double.parseDouble(salaryField.getText());
            if (Double.parseDouble(salaryField.getText()) < 0) {
                salaryField.setBackground(bg);
                valid = false;
            }
        }
        catch (NumberFormatException num) {
            if (salaryField.isEditable()) {
                salaryField.setBackground(bg);
                valid = false;
            }
        }
        if (fullTimeCombo.getSelectedIndex() == 0 && fullTimeCombo.isEnabled()) {
            fullTimeCombo.setBackground(bg);
            valid = false;
        }
        if (!valid)
            JOptionPane.showMessageDialog(null, "Wrong values or format! Please check!");
        if (ppsField.isEditable())
            setToWhite();

        return valid;
    }

    private void setToWhite() {
        final String color = "TextField.background";
        ppsField.setBackground(UIManager.getColor(color));
        surnameField.setBackground(UIManager.getColor(color));
        firstNameField.setBackground(UIManager.getColor(color));
        salaryField.setBackground(UIManager.getColor(color));
        genderCombo.setBackground(UIManager.getColor(color));
        departmentCombo.setBackground(UIManager.getColor(color));
        fullTimeCombo.setBackground(UIManager.getColor(color));
    }

    public void setEnabled(boolean booleanValue) {
        boolean search;
        search = !booleanValue;
        ppsField.setEditable(booleanValue);
        surnameField.setEditable(booleanValue);
        firstNameField.setEditable(booleanValue);
        genderCombo.setEnabled(booleanValue);
        departmentCombo.setEnabled(booleanValue);
        salaryField.setEditable(booleanValue);
        fullTimeCombo.setEnabled(booleanValue);
        saveChange.setVisible(booleanValue);
        cancelChange.setVisible(booleanValue);
        searchByIdField.setEnabled(search);
        searchBySurnameField.setEnabled(search);
        searchId.setEnabled(search);
        searchSurname.setEnabled(search);
    }
    private void openFile() {
        final JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Open");
        fc.setFileFilter(dataFilter);
        saveOldFile();
        int returnVal = fc.showOpenDialog(EmployeeDetails.this);
        openFile(fc, returnVal);
    }

    private void openFile(JFileChooser fc, int returnVal) {
        File newFile;
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            newFile = fc.getSelectedFile();
            if (file.getName().equals(generatedFileName))
                file.delete();
            file = newFile;
            application.openReadFile(file.getAbsolutePath());
            firstRecord();
            displayRecords(currentEmployee);
            application.closeReadFile();
        }
    }

    private void saveOldFile() {
        if (file.length() != 0 || change) {
            int returnVal = JOptionPane.showOptionDialog(frame, "Do you want to save changes?", "Save",
                    JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
            if (returnVal == JOptionPane.YES_OPTION) {
                saveFile();
            }
        }
    }
    private void saveFile() {
        if (file.getName().equals(generatedFileName))
            saveFileAs();
        else {
            saveTextField();
            displayRecords(currentEmployee);
            setEnabled(false);
        }
    }
    private void saveTextField() {
        if (change) {
            int returnVal = JOptionPane.showOptionDialog(frame, "Do you want to save changes?", "Save",
                    JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
            if (returnVal == JOptionPane.YES_OPTION) {
                if (!idField.getText().equals("")) {
                    application.openWriteFile(file.getAbsolutePath());
                    currentEmployee = getChangedDetails();
                    application.changeRecords(currentEmployee, currentByteStart);
                    application.closeWriteFile();
                }
            }
        }
    }

    private void saveChanges() {
        int returnVal = JOptionPane.showOptionDialog(frame, "Do you want to save changes to current Employee?", "Save",
                JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
        if (returnVal == JOptionPane.YES_OPTION) {
            application.openWriteFile(file.getAbsolutePath());
            currentEmployee = getChangedDetails();
            application.changeRecords(currentEmployee, currentByteStart);
            application.closeWriteFile();
            changesMade = false;
        }
        displayRecords(currentEmployee);
        setEnabled(false);
    }

    private void saveFileAs() {
        final JFileChooser fc = new JFileChooser();
        String defaultFileName = "new_Employee.dat";
        fc.setDialogTitle("Save As");
        fc.setFileFilter(dataFilter);
        fc.setApproveButtonText("Save");
        fc.setSelectedFile(new File(defaultFileName));
        int returnVal = fc.showSaveDialog(EmployeeDetails.this);
        saveToFile(fc, returnVal);
        changesMade = false;
    }

    private void saveToFile(JFileChooser fc, int returnVal) {
        File newFile;
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            newFile = fc.getSelectedFile();
            newFile = checkFileNameExists(newFile);
            try {
                Files.copy(file.toPath(), newFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                if (file.getName().equals(generatedFileName))
                    file.delete();
                file = newFile;
            }
            catch (IOException ignored) {
            }
        }
    }

    private File checkFileNameExists(File newFile) {
        if (!checkFileName(newFile)) {
            newFile = new File(newFile.getAbsolutePath() + ".dat");
            application.createFile(newFile.getAbsolutePath());
        }
        else
            application.createFile(newFile.getAbsolutePath());
        return newFile;
    }

    private void exitApp() {
        if (file.length() != 0) {
            changesMade();
        } else {
            if (file.getName().equals(generatedFileName))
                file.delete();
            System.exit(0);
        }
    }
    private void changesMade() {
        if (changesMade) {
            int returnVal = JOptionPane.showOptionDialog(frame, "Do you want to save changes?", "Save",
                    JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
                    saveOrNot(returnVal);
        }
        else {
            if (file.getName().equals(generatedFileName))
                file.delete();
            System.exit(0);
        }
    }

    private void saveOrNot(int returnVal) {
        if (returnVal == JOptionPane.YES_OPTION) {
            saveFile();
            if (file.getName().equals(generatedFileName))
                file.delete();
            System.exit(0);
        }
        else if (returnVal == JOptionPane.NO_OPTION) {
            if (file.getName().equals(generatedFileName))
                file.delete();
            System.exit(0);
        }
    }

    private String getFileName() {
        String fileNameChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_-";
        StringBuilder fileName = new StringBuilder();
        Random rnd = new Random();
        while (fileName.length() < 20) {
            int index = (int) (rnd.nextFloat() * fileNameChars.length());
            fileName.append(fileNameChars.charAt(index));
        }
        return fileName.toString();
    }

    private void createRandomFile() {
        generatedFileName = getFileName() + ".dat";
        file = new File(generatedFileName);
        application.createFile(file.getName());
    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == closeApp) {
            if (checkInput() && !checkForChanges())
                exitApp();
        } else if (e.getSource() == open) {
            if (checkInput() && !checkForChanges())
                openFile();
        } else if (e.getSource() == save) {
            if (checkInput() && !checkForChanges())
                saveFile();
            change = false;
        } else if (e.getSource() == saveAs) {
            if (checkInput() && !checkForChanges())
                saveFileAs();
            change = false;
        } else if (e.getSource() == searchById) {
            if (checkInput() && !checkForChanges())
                displaySearchByIdDialog();
        } else if (e.getSource() == searchBySurname) {
            if (checkInput() && !checkForChanges())
                displaySearchBySurnameDialog();
        } else if (e.getSource() == searchId || e.getSource() == searchByIdField)
            searchEmployeeById();
        else if (e.getSource() == searchSurname || e.getSource() == searchBySurnameField)
            searchEmployeeBySurname();
        else if (e.getSource() == saveChange) {
            if (checkInput() && !checkForChanges())
                ;
        } else if (e.getSource() == cancelChange)
            cancelChange();
        else if (e.getSource() == firstItem || e.getSource() == first) {
            if (checkInput() && !checkForChanges()) {
                firstRecord();
                displayRecords(currentEmployee);
            }
        } else if (e.getSource() == prevItem || e.getSource() == previous) {
            if (checkInput() && !checkForChanges()) {
                previousRecord();
                displayRecords(currentEmployee);
            }
        } else if (e.getSource() == nextItem || e.getSource() == next) {
            if (checkInput() && !checkForChanges()) {
                nextRecord();
                displayRecords(currentEmployee);
            }
        } else if (e.getSource() == lastItem || e.getSource() == last) {
            if (checkInput() && !checkForChanges()) {
                lastRecord();
                displayRecords(currentEmployee);
            }
        } else if (e.getSource() == listAll || e.getSource() == displayAll) {
            if (checkInput() && !checkForChanges())
                if (isSomeoneToDisplay())
                    displayEmployeeSummaryDialog();
        } else if (e.getSource() == create || e.getSource() == add) {
            if (checkInput() && !checkForChanges()) new AddRecordDialog(EmployeeDetails.this);
        } else if (e.getSource() == modify || e.getSource() == edit) {
            if (checkInput() && !checkForChanges())
                editDetails();
        } else if (e.getSource() == delete || e.getSource() == deleteButton) {
            if (checkInput() && !checkForChanges())
                deleteRecord();
        } else if (e.getSource() == searchBySurname) {
            if (checkInput() && !checkForChanges())
                new SearchBySurnameDialog(EmployeeDetails.this);
        }
    }

    private void createContentPane() {
        setTitle("Employee Details");
        createRandomFile();
        JPanel dialog = new JPanel(new MigLayout());
        setJMenuBar(menuBar());
        dialog.add(searchPanel(), "width 400:400:400, grow, push");
        dialog.add(navigPanel(), "width 150:150:150, wrap");
        dialog.add(buttonPanel(), "grow, push, span 2,wrap");
        dialog.add(detailsPanel(), "gap top 30, gap left 150, center");
        JScrollPane scrollPane = new JScrollPane(dialog);
        getContentPane().add(scrollPane, BorderLayout.CENTER);
        addWindowListener(this);
    }

    private static void createAndShowGUI() {
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.createContentPane();
        frame.setSize(760, 600);
        frame.setLocation(250, 200);
        frame.setVisible(true);
    }


    public static void main(String args[]) {
        javax.swing.SwingUtilities.invokeLater(EmployeeDetails::createAndShowGUI);
    }

    public void changedUpdate(DocumentEvent d) {
        change = true;
        new JTextFieldLimit(20);
    }

    public void insertUpdate(DocumentEvent d) {
        change = true;
        new JTextFieldLimit(20);
    }

    public void removeUpdate(DocumentEvent d) {
        change = true;
        new JTextFieldLimit(20);
    }

    public void itemStateChanged(ItemEvent e) {
        change = true;
    }

    public void windowClosing(WindowEvent e) {
        exitApp();
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowClosed(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowOpened(WindowEvent e) {
    }
}
