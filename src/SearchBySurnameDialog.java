

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


 class SearchBySurnameDialog extends JDialog  implements ActionListener {
	 private final EmployeeDetails parent;
     private JButton search;
     private JButton cancel;
     private JTextField searchField;
     private final JPanel searchPanel = new JPanel(new GridLayout(3, 1));
     private final  JPanel textPanel = new JPanel();
     private final  JPanel buttonPanel = new JPanel();

	public SearchBySurnameDialog(EmployeeDetails parent) {
		setTitle("Search by Surname");
		setModal(true);
		this.parent = parent;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JScrollPane scrollPane = new JScrollPane(searchPane());
		setContentPane(scrollPane);
		getRootPane().setDefaultButton(search);
		setSize(500, 190);
		setLocation(350, 250);
		setVisible(true);
	}

    private Container searchPane() {

        JLabel searchLabel;
        searchPanel.add(new JLabel("Search by Surname"));
        textPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        textPanel.add(searchLabel = new JLabel("Enter Surname:"));
        searchLabel.setFont(this.parent.font1);
        textPanel.add(searchField = new JTextField(20));
        searchField.setFont(this.parent.font1);
        searchField.setDocument(new JTextFieldLimit(20));
        buttonPanel.add(search = new JButton("Search"));
        search.addActionListener(this);
        search.requestFocus();
        buttonPanel.add(cancel = new JButton("Cancel"));
        cancel.addActionListener(this);
        searchPanel.add(textPanel);
        searchPanel.add(buttonPanel);
        return searchPanel;

    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == search){
            this.parent.searchBySurnameField.setText(searchField.getText());
            this.parent.searchEmployeeBySurname();
            dispose();
        }
        else if(e.getSource() == cancel)
            dispose();
    }

}
