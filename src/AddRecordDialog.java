/*
 * 
 * This is a dialog for adding new Employees and saving records to file
 * 
 * */

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddRecordDialog extends JDialog implements ActionListener {
	JTextField idField;
    JTextField ppsField;
    JTextField surnameField;
    JTextField firstNameField;
    JTextField salaryField;
	JComboBox<String> genderCombo;
    JComboBox<String> departmentCombo;
    JComboBox<String> fullTimeCombo;
	JButton save;
    JButton cancel;
	EmployeeDetails parent;
     Color white = Color.WHITE;

	public AddRecordDialog(EmployeeDetails parent) {
		setTitle("Add Record");
		setModal(true);
		this.parent = parent;
		this.parent.setEnabled(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JScrollPane scrollPane = new JScrollPane(dialogPane());
		setContentPane(scrollPane);
		getRootPane().setDefaultButton(save);
		setSize(500, 370);
		setLocation(350, 250);
		setVisible(true);
	}

	public Container dialogPane() {
		JPanel empDetails, buttonPanel;
		empDetails = new JPanel(new MigLayout());
		buttonPanel = new JPanel();
        getButtonPanel(buttonPanel);
        addEmpDetails(empDetails, buttonPanel);
		return empDetails;
	}

    void getButtonPanel(JPanel buttonPanel) {
        buttonPanel.add(save = new JButton("Save"));
        save.addActionListener(this);
        save.requestFocus();
        buttonPanel.add(cancel = new JButton("Cancel"));
        cancel.addActionListener(this);
    }

    void addEmpDetails(JPanel empDetails, JPanel buttonPanel) {
        empDetails.setBorder(BorderFactory.createTitledBorder("Employee Details"));
        final String constraints1 = "growx, pushx";
        empDetails.add(new JLabel("ID:"), constraints1);
        final String constraints = "growx, pushx, wrap";
        empDetails.add(idField = new JTextField(20), constraints);
        idField.setEditable(false);
        empDetails.add(new JLabel("PPS Number:"), constraints1);
        empDetails.add(ppsField = new JTextField(20), constraints);
        empDetails.add(new JLabel("Surname:"), constraints1);
        empDetails.add(surnameField = new JTextField(20), constraints);
        empDetails.add(new JLabel("First Name:"), constraints1);
        empDetails.add(firstNameField = new JTextField(20), constraints);
        empDetails.add(new JLabel("Gender:"), constraints1);
        empDetails.add(genderCombo = new JComboBox<String>(this.parent.gender), constraints);
        empDetails.add(new JLabel("Department:"), constraints1);
        empDetails.add(departmentCombo = new JComboBox<String>(this.parent.department), constraints);
        empDetails.add(new JLabel("Salary:"), constraints1);
        empDetails.add(salaryField = new JTextField(20), constraints);
        empDetails.add(new JLabel("Full Time:"), constraints1);
        empDetails.add(fullTimeCombo = new JComboBox<String>(this.parent.fullTime), constraints);
        empDetails.add(buttonPanel, "span 2,growx, pushx,wrap");
        for (int i = 0; i < empDetails.getComponentCount(); i++) {
            empDetails.getComponent(i).setFont(this.parent.font1);
            addFonts(empDetails, i);
        }
        idField.setText(Integer.toString(this.parent.getNextFreeId()));
    }

    private void addFonts(JPanel empDetails, int i) {
        JTextField field;
        final Component component = empDetails.getComponent(i);
        
        if (component instanceof JComboBox) {

            component.setBackground(white);
        }
        else if(component instanceof JTextField){
            field = (JTextField) component;
            if(field == ppsField)
                field.setDocument(new JTextFieldLimit(9));
            else
            field.setDocument(new JTextFieldLimit(20));
        }
    }

	public void addRecord() {
		boolean fullTime = false;
		Employee theEmployee;

		if (((String) fullTimeCombo.getSelectedItem()).equalsIgnoreCase("Yes"))
			fullTime = true;
		theEmployee = new Employee(Integer.parseInt(idField.getText()), ppsField.getText().toUpperCase(), surnameField.getText().toUpperCase(),
				firstNameField.getText().toUpperCase(), genderCombo.getSelectedItem().toString().charAt(0), new Job( departmentCombo.getSelectedItem().toString(),
                 Double.parseDouble(salaryField.getText()), fullTime));
		this.parent.currentEmployee = theEmployee;
		this.parent.addRecord(theEmployee);
		this.parent.displayRecords(theEmployee);
	}

	public boolean checkInput() {
		boolean valid = true;
		if (ppsField.getText().equals("")) {
			ppsField.setBackground(new Color(255, 150, 150));
			valid = false;
		}
		if (this.parent.correctPps(this.ppsField.getText().trim(), -1)) {
			ppsField.setBackground(new Color(255, 150, 150));
			valid = false;
		}
		if (surnameField.getText().isEmpty()) {
			surnameField.setBackground(new Color(255, 150, 150));
			valid = false;
		}
		if (firstNameField.getText().isEmpty()) {
			firstNameField.setBackground(new Color(255, 150, 150));
			valid = false;
		}
		if (genderCombo.getSelectedIndex() == 0) {
			genderCombo.setBackground(new Color(255, 150, 150));
			valid = false;
		}
		if (departmentCombo.getSelectedIndex() == 0) {
			departmentCombo.setBackground(new Color(255, 150, 150));
			valid = false;
		}
		try {
			Double.parseDouble(salaryField.getText());
			if (Double.parseDouble(salaryField.getText()) < 0) {
				salaryField.setBackground(new Color(255, 150, 150));
				valid = false;
			}
		}
		catch (NumberFormatException num) {
			salaryField.setBackground(new Color(255, 150, 150));
			valid = false;
		}
		if (fullTimeCombo.getSelectedIndex() == 0) {
			fullTimeCombo.setBackground(new Color(255, 150, 150));
			valid = false;
		}
		return valid;
	}

	public void setToWhite() {
        ppsField.setBackground(white);
        surnameField.setBackground(white);
        firstNameField.setBackground(white);
        salaryField.setBackground(white);
        genderCombo.setBackground(white);
        departmentCombo.setBackground(white);
        fullTimeCombo.setBackground(white);
    }

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == save) {
            checkInputs();
        }
		else if (e.getSource() == cancel)
			dispose();
	}

    void checkInputs() {
        if (checkInput()) {
            addRecord();
            dispose();
            this.parent.changesMade = true;
        }
        else {
            JOptionPane.showMessageDialog(null, "Wrong values or format! Please check!");
            setToWhite();
        }
    }
}