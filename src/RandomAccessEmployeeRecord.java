/*
 * 
 * This is a Random Access Employee record definition
 * 
 * */

import java.io.RandomAccessFile;
import java.io.IOException;

@SuppressWarnings("ALL")
class RandomAccessEmployeeRecord extends Employee
{

    public static final int SIZE = 175;

   public RandomAccessEmployeeRecord() {
      this(0, "","","",'\0', null);
   }

   public RandomAccessEmployeeRecord( int employeeId, String pps, String surname, String firstName, char gender, Job job) {
            super(employeeId, pps, surname, firstName, gender,  job);
   }

   public void read( RandomAccessFile file ) throws IOException
   {    setEmployeeId(file.readInt());
		setPps(readName(file));
		setSurname(readName(file));
		setFirstName(readName(file));
		setGender(file.readChar());
        Job job = new Job(readName(file),file.readDouble(), file.readBoolean());
        setJob(job);
   }

   private String readName( RandomAccessFile file ) throws IOException
   {
      char name[] = new char[ 20 ], temp;
      for ( int count = 0; count < name.length; count++ )
      {
         temp = file.readChar();
         name[ count ] = temp;
      }
      return new String( name ).replace( '\0', ' ' );
   }

   public void write( RandomAccessFile file ) throws IOException
   {
      file.writeInt( getEmployeeId() );
      writeName(file, getPps().toUpperCase());
      writeName( file, getSurname().toUpperCase() );
      writeName( file, getFirstName().toUpperCase() );
      file.writeChar(getGender());
      writeName(file, getJob().getDepartment());
      file.writeDouble( getJob().getSalary() );
      file.writeBoolean(getJob().getFullTime());
   }

   private void writeName( RandomAccessFile file, String name )
      throws IOException
   {
      StringBuffer buffer = null;
      if ( name != null ) 
         buffer = new StringBuffer( name );
      else 
         buffer = new StringBuffer( 20 );

      buffer.setLength( 20 );
      file.writeChars( buffer.toString() );
   }
}