/*
 * 
 * This is the definition of the Employee object
 * 
 * */

public class Employee{
	private int employeeId;
	private String pps;
	private String surname;
	private String firstName;
	private char gender;
    private Job job;
    
	Employee() {
		this.employeeId = 0;
		this.pps = "";
		this.surname = "";
		this.firstName = "";
		this.gender = '\0';

	}
	public Employee(int employeeId, String pps, String surname, String firstName, char gender, Job job) {
		this.employeeId = employeeId;
		this.pps = pps;
		this.surname = surname;
		this.firstName = firstName;
		this.gender = gender;
        this.job = job;

	}
	public int getEmployeeId() {
		return this.employeeId;
	}
	public String getPps() {
		return pps;
	}
    public String getSurname() {
		return this.surname;
	}
    public String getFirstName() {
		return this.firstName;
	}
    public char getGender() {
		return this.gender;
	}
    public Job getJob() {
        return job;
    }
    void setJob(Job job) {
        this.job = job;
    }
    void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
    void setPps(String pps) {
		this.pps = pps;
	}
    void setSurname(String surname) {
		this.surname = surname;
	}
    void setFirstName(String firstName) {
		this.firstName = firstName;
	}
    void setGender(char gender) {
		this.gender = gender;
	}

	public String toString() {
		return "Employee ID: " + this.employeeId + "\nPPS Number: " + this.pps + "\nSurname: " + this.surname
				+ "\nFirst Name: " + this.firstName + "\nGender: " + this.gender + "\nJob: " + this.job ;
	}

}

class Job{

    private String department;
    private double salary;
    private boolean fullTime;

    public Job() {
        this.department = "";
        this.salary = 0;
        this.fullTime = false;
    }
    public Job(String department, double salary, boolean fullTime) {
        this.department = department;
        this.salary = salary;
        this.fullTime = fullTime;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public boolean getFullTime() {
        return fullTime;
    }

    public void setFullTime(boolean fullTime) {
        this.fullTime = fullTime;
    }

    @Override
    public String toString() {
        @SuppressWarnings("UnusedAssignment") String bool = "";
        if (fullTime)
            bool = "Yes";
        else
            bool = "No";

        return "Job{" +
                "\nDepartment='" + this.department + '\''
                + "\nSalary: " + this.salary
                + "\nFull Time: " + bool;
    }
}