import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;

public class EmployeeDetailsTest {
    Employee employee, employee2, employee3;
    List<Employee> employeeList = new ArrayList<>();

    @Before
    public void setUp() throws Exception {

        employee = new Employee(4, "1234567W", "ALMA" ,"Haya", 'F', new Job("Production", 23456 ,true ));
        employee2 = new Employee(2, "1233567W", "SMITH" ,"Sam", 'M', new Job("Production", 23456 ,true ));
        employeeList.add(employee);
        employeeList.add(employee2);
    }
    @Test
    public void addRecordTest(){
        employee3 = new Employee(3, "1232267W", "JOHNS" ,"JJ", 'M', new Job("TRANSPORT", 22456 ,true ));
        employeeList.add(employee3);
        assertTrue(employeeList.contains(employee3));

    }
    @Test
    public void searchEmployeeById() throws Exception {
        assertEquals(2, employee2.getEmployeeId());
    }
    @Test
    public void searchEmployeeBySurname() throws Exception {
        assertEquals("SMITH", employee2.getSurname());
    }

    @Test
    public void editEmployee() throws Exception {
        for(int i=0; i<employeeList.size(); i++) {
            assertTrue(employeeList.contains(employee));
            employee.setFirstName("SARA");
            employee.getJob().setSalary(33457);
            assertEquals("SARA", employee.getFirstName());
            assertTrue(employeeList.contains(employee));
        }
    }

    @Test
    public void deleteEmployee() throws Exception {
        employeeList.remove(employee);
        for(int i=0; i<employeeList.size(); i++) {
            assertFalse(employee == employeeList.get(i));
        }
    }

}